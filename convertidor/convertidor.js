function decimalToRoman(decimal) {
    let romanValue = '';
    let temp = decimal;

    const numeroSimbolo = [
        { d: 100, r: 'C' },
        { d: 90, r: 'XC' },
        { d: 50, r: 'L' },
        { d: 40, r: 'XL' },
        { d: 10, r: 'X' },
        { d: 9, r: 'IX' },
        { d: 5, r: 'V' },
        { d: 4, r: 'IV' },
        { d: 1, r: 'I' }
    ];

    for (let i = 0; i < numeroSimbolo.length; i++) {
        while (temp >= numeroSimbolo[i].d) {
            temp = temp - numeroSimbolo[i].d;
            romanValue += numeroSimbolo[i].r;
        }
    }
    return romanValue;
}

const simbolValue = { 'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100 }

function calc1(romanoAux) {
    return simbolValue[romanoAux[0]];
}

function calc2(romanoAux) {
    let contador = 0;
    let val1 = calc1(romanoAux[0]);
    let val2 = calc1(romanoAux[1]);
    if (val1 < val2)
        contador = val2 - val1;
    else
        contador = val1 + val2;
    return contador;
}

function calc3(romanoAux) {
    let contador = 0;
    let val1 = calc1(romanoAux[0]);
    let val2 = calc1(romanoAux[1]);
    let val3 = calc1(romanoAux[2]);
    if (val1 >= val2)
        contador = val1 + calc2([romanoAux[1], romanoAux[2]]);
    else
        contador = val2 - val1 + val3;
    return contador;
}
function splitRoman(romanoAux) {
    const even = romanoAux.length % 2
    const limit = romanoAux.length / 2;
    const part = even == 0 ? limit : limit + 1;

    const part1 = romanoAux.slice(0, part);
    const part2 = romanoAux.slice(part, romanoAux.length);

    return [part1, part2];
}

function calc4(romanoAux) {
    const parts = splitRoman(romanoAux.slice(0,4))
    return calc2(parts[0]) + calc2(parts[1]);
}

function calc5(romanoAux) {
    const parts = splitRoman(romanoAux.slice(0,5))
    return calc3(parts[0]) + calc2(parts[1]);
}

function calc6(romanoAux) {
    const parts = splitRoman(romanoAux.slice(0,6))
    return calc3(parts[0]) + calc3(parts[1]);
}

function calc7(romanoAux) {
    const parts = splitRoman(romanoAux.slice(0,7))
    return calc4(parts[0]) + calc3(parts[1]);
}

function calc8(romanoAux) {
    const parts = splitRoman(romanoAux.slice(0,8))
    return calc4(parts[0]) + calc4(parts[1]);
}




function romanToDecimal(roman) {
    let contador = 0;
    let romanoAux = roman.split("");

    if (romanoAux.length === 1) {
        contador = calc1(romanoAux);
    }
    if (romanoAux.length === 2) {
        contador = calc2(romanoAux);
    }
    if (romanoAux.length === 3) {
        contador = calc3(romanoAux);
    }
    if (romanoAux.length === 4) {
        contador = calc4(romanoAux);
    }
    if (romanoAux.length === 5) {
        contador = calc5(romanoAux);
    }
    if (romanoAux.length === 6) {
        contador = calc6(romanoAux);
    }
    if (romanoAux.length === 7) {
        contador = calc7(romanoAux);
    }
    if (romanoAux.length === 8) {
        contador = calc8(romanoAux);
    }
    return contador

}
module.exports = {
    decimalToRoman: decimalToRoman,
    romanToDecimal: romanToDecimal,
    splitRoman: splitRoman,
}






