const convertidor = require('./convertidor.js');

describe('Convertidor de numeros ', () => {
    describe('decimales a romanos ', () => {
        function checkDecimaltoRoman(decimal, roman) {
            test('decimal ' + decimal + ' es ' + roman + ' romano', () => {
                expect(convertidor.decimalToRoman(decimal)).toBe(roman);
            });
        }

        checkDecimaltoRoman(1, 'I');
        checkDecimaltoRoman(2, 'II');
        checkDecimaltoRoman(3, 'III');
        checkDecimaltoRoman(4, 'IV');
        checkDecimaltoRoman(5, 'V');
        checkDecimaltoRoman(6, 'VI');
        checkDecimaltoRoman(7, 'VII');
        checkDecimaltoRoman(8, 'VIII');
        checkDecimaltoRoman(9, 'IX');
        checkDecimaltoRoman(10, 'X');
        checkDecimaltoRoman(11, 'XI');
        checkDecimaltoRoman(12, 'XII');
        checkDecimaltoRoman(13, 'XIII');
        checkDecimaltoRoman(14, 'XIV');
        checkDecimaltoRoman(15, 'XV');
        checkDecimaltoRoman(16, 'XVI');
        checkDecimaltoRoman(17, 'XVII');
        checkDecimaltoRoman(18, 'XVIII');
        checkDecimaltoRoman(19, 'XIX');
        checkDecimaltoRoman(20, 'XX');
        checkDecimaltoRoman(21, 'XXI');
        checkDecimaltoRoman(22, 'XXII');
        checkDecimaltoRoman(23, 'XXIII');
        checkDecimaltoRoman(24, 'XXIV');
        checkDecimaltoRoman(25, 'XXV');
        checkDecimaltoRoman(26, 'XXVI');
        checkDecimaltoRoman(27, 'XXVII');
        checkDecimaltoRoman(28, 'XXVIII');
        checkDecimaltoRoman(29, 'XXIX');
        checkDecimaltoRoman(30, 'XXX');
        checkDecimaltoRoman(31, 'XXXI');
        checkDecimaltoRoman(32, 'XXXII');
        checkDecimaltoRoman(33, 'XXXIII');
        checkDecimaltoRoman(34, 'XXXIV');
        checkDecimaltoRoman(35, 'XXXV');
        checkDecimaltoRoman(36, 'XXXVI');
        checkDecimaltoRoman(37, 'XXXVII');
        checkDecimaltoRoman(38, 'XXXVIII');
        checkDecimaltoRoman(39, 'XXXIX');
        checkDecimaltoRoman(40, 'XL');
        checkDecimaltoRoman(41, 'XLI');
        checkDecimaltoRoman(42, 'XLII');
        checkDecimaltoRoman(43, 'XLIII');
        checkDecimaltoRoman(44, 'XLIV');
        checkDecimaltoRoman(45, 'XLV');
        checkDecimaltoRoman(46, 'XLVI');
        checkDecimaltoRoman(47, 'XLVII');
        checkDecimaltoRoman(48, 'XLVIII');
        checkDecimaltoRoman(49, 'XLIX');
        checkDecimaltoRoman(50, 'L');
        checkDecimaltoRoman(51, 'LI');
        checkDecimaltoRoman(52, 'LII');
        checkDecimaltoRoman(53, 'LIII');
        checkDecimaltoRoman(54, 'LIV');
        checkDecimaltoRoman(55, 'LV');
        checkDecimaltoRoman(56, 'LVI');
        checkDecimaltoRoman(57, 'LVII');
        checkDecimaltoRoman(58, 'LVIII');
        checkDecimaltoRoman(59, 'LIX');
        checkDecimaltoRoman(60, 'LX');
        checkDecimaltoRoman(61, 'LXI');
        checkDecimaltoRoman(62, 'LXII');
        checkDecimaltoRoman(63, 'LXIII');
        checkDecimaltoRoman(64, 'LXIV');
        checkDecimaltoRoman(65, 'LXV');
        checkDecimaltoRoman(66, 'LXVI');
        checkDecimaltoRoman(67, 'LXVII');
        checkDecimaltoRoman(68, 'LXVIII');
        checkDecimaltoRoman(69, 'LXIX');
        checkDecimaltoRoman(70, 'LXX');
        checkDecimaltoRoman(71, 'LXXI');
        checkDecimaltoRoman(72, 'LXXII');
        checkDecimaltoRoman(73, 'LXXIII');
        checkDecimaltoRoman(74, 'LXXIV');
        checkDecimaltoRoman(75, 'LXXV');
        checkDecimaltoRoman(76, 'LXXVI');
        checkDecimaltoRoman(77, 'LXXVII');
        checkDecimaltoRoman(78, 'LXXVIII');
        checkDecimaltoRoman(79, 'LXXIX');
        checkDecimaltoRoman(80, 'LXXX');
        checkDecimaltoRoman(81, 'LXXXI');
        checkDecimaltoRoman(82, 'LXXXII');
        checkDecimaltoRoman(83, 'LXXXIII');
        checkDecimaltoRoman(84, 'LXXXIV');
        checkDecimaltoRoman(85, 'LXXXV');
        checkDecimaltoRoman(86, 'LXXXVI');
        checkDecimaltoRoman(87, 'LXXXVII');
        checkDecimaltoRoman(88, 'LXXXVIII');
        checkDecimaltoRoman(89, 'LXXXIX');
        checkDecimaltoRoman(90, 'XC');
        checkDecimaltoRoman(91, 'XCI');
        checkDecimaltoRoman(92, 'XCII');
        checkDecimaltoRoman(93, 'XCIII');
        checkDecimaltoRoman(94, 'XCIV');
        checkDecimaltoRoman(95, 'XCV');
        checkDecimaltoRoman(96, 'XCVI');
        checkDecimaltoRoman(97, 'XCVII');
        checkDecimaltoRoman(98, 'XCVIII');
        checkDecimaltoRoman(99, 'XCIX');
        checkDecimaltoRoman(100, 'C');
    });

    describe('romanos a decimales', () => {
        function checkRomanToDecimal(decimal, roman) {
            test('romano ' + roman + ' es ' + decimal + ' decimal', () => {
                expect(convertidor.romanToDecimal(roman)).toBe(decimal);
            });
        }

        checkRomanToDecimal(1, 'I');
        checkRomanToDecimal(2, 'II');
        checkRomanToDecimal(3, 'III');
        checkRomanToDecimal(4, 'IV');
        checkRomanToDecimal(5, 'V');
        checkRomanToDecimal(6, 'VI');
        checkRomanToDecimal(7, 'VII');
        checkRomanToDecimal(8, 'VIII');
        checkRomanToDecimal(9, 'IX');
        checkRomanToDecimal(10, 'X');
        checkRomanToDecimal(11, 'XI');
        checkRomanToDecimal(12, 'XII');
        checkRomanToDecimal(13, 'XIII');
        checkRomanToDecimal(14, 'XIV');
        checkRomanToDecimal(15, 'XV');
        checkRomanToDecimal(16, 'XVI');
        checkRomanToDecimal(17, 'XVII');
        checkRomanToDecimal(18, 'XVIII');
        checkRomanToDecimal(19, 'XIX');
        checkRomanToDecimal(20, 'XX');
        checkRomanToDecimal(21, 'XXI');
        checkRomanToDecimal(22, 'XXII');
        checkRomanToDecimal(23, 'XXIII');
        checkRomanToDecimal(24, 'XXIV');
        checkRomanToDecimal(25, 'XXV');
        checkRomanToDecimal(26, 'XXVI');
        checkRomanToDecimal(27, 'XXVII');
        checkRomanToDecimal(28, 'XXVIII');
        checkRomanToDecimal(29, 'XXIX');
        checkRomanToDecimal(30, 'XXX');
        checkRomanToDecimal(31, 'XXXI');
        checkRomanToDecimal(32, 'XXXII');
        checkRomanToDecimal(33, 'XXXIII');
        checkRomanToDecimal(34, 'XXXIV');
        checkRomanToDecimal(35, 'XXXV');
        checkRomanToDecimal(36, 'XXXVI');
        checkRomanToDecimal(37, 'XXXVII');
        checkRomanToDecimal(38, 'XXXVIII');
        checkRomanToDecimal(39, 'XXXIX');
        checkRomanToDecimal(40, 'XL');
        checkRomanToDecimal(41, 'XLI');
        checkRomanToDecimal(42, 'XLII');
        checkRomanToDecimal(43, 'XLIII');
        checkRomanToDecimal(44, 'XLIV');
        checkRomanToDecimal(45, 'XLV');
        checkRomanToDecimal(46, 'XLVI');
        checkRomanToDecimal(47, 'XLVII');
        checkRomanToDecimal(48, 'XLVIII');
        checkRomanToDecimal(49, 'XLIX');
        checkRomanToDecimal(50, 'L');
        checkRomanToDecimal(51, 'LI');
        checkRomanToDecimal(52, 'LII');
        checkRomanToDecimal(53, 'LIII');
        checkRomanToDecimal(54, 'LIV');
        checkRomanToDecimal(55, 'LV');
        checkRomanToDecimal(56, 'LVI');
        checkRomanToDecimal(57, 'LVII');
        checkRomanToDecimal(58, 'LVIII');
        checkRomanToDecimal(59, 'LIX');
        checkRomanToDecimal(60, 'LX');
        checkRomanToDecimal(61, 'LXI');
        checkRomanToDecimal(62, 'LXII');
        checkRomanToDecimal(63, 'LXIII');
        checkRomanToDecimal(64, 'LXIV');
        checkRomanToDecimal(65, 'LXV');
        checkRomanToDecimal(66, 'LXVI');
        checkRomanToDecimal(67, 'LXVII');
        checkRomanToDecimal(68, 'LXVIII');
        checkRomanToDecimal(69, 'LXIX');
        checkRomanToDecimal(70, 'LXX');
        checkRomanToDecimal(71, 'LXXI');
        checkRomanToDecimal(72, 'LXXII');
        checkRomanToDecimal(73, 'LXXIII');
        checkRomanToDecimal(74, 'LXXIV');
        checkRomanToDecimal(75, 'LXXV');
        checkRomanToDecimal(76, 'LXXVI');
        checkRomanToDecimal(77, 'LXXVII');
        checkRomanToDecimal(78, 'LXXVIII');
        checkRomanToDecimal(79, 'LXXIX');
        checkRomanToDecimal(80, 'LXXX');
        checkRomanToDecimal(81, 'LXXXI');
        checkRomanToDecimal(82, 'LXXXII');
        checkRomanToDecimal(83, 'LXXXIII');
        checkRomanToDecimal(84, 'LXXXIV');
        checkRomanToDecimal(85, 'LXXXV');
        checkRomanToDecimal(86, 'LXXXVI');
        checkRomanToDecimal(87, 'LXXXVII');
        checkRomanToDecimal(88, 'LXXXVIII');
        checkRomanToDecimal(89, 'LXXXIX');
        checkRomanToDecimal(90, 'XC');
        checkRomanToDecimal(91, 'XCI');
        checkRomanToDecimal(92, 'XCII');
        checkRomanToDecimal(93, 'XCIII');
        checkRomanToDecimal(94, 'XCIV');
        checkRomanToDecimal(95, 'XCV');
        checkRomanToDecimal(96, 'XCVI');
        checkRomanToDecimal(97, 'XCVII');
        checkRomanToDecimal(98, 'XCVIII');
        checkRomanToDecimal(99, 'XCIX');
        checkRomanToDecimal(100, 'C');
    });


    describe('split romanos', () => {
        function splitRomano(input, part1, part2) {
            test('romano ' + input + ' es ' + part1 + ' y ' + part2, () => {
                expect(convertidor.splitRoman(input)[0]).toEqual(part1);
                expect(convertidor.splitRoman(input)[1]).toEqual(part2);
            });
        };

        splitRomano(['1'], ['1'], [])
        splitRomano(['1', '2'], ['1'], ['2'])
        splitRomano(['1', '2', '3'], ['1', '2'], ['3'])
        splitRomano(['1', '2', '3', '4'], ['1', '2'], ['3', '4'])
        splitRomano(['1', '2', '3', '4', '5', '6', '7', '8', '9'], ['1', '2', '3', '4', '5'], ['6', '7', '8', '9'])
    });
});


