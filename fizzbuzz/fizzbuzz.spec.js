//fiZZBUZZ 1 => 1, 2=> 2, 3 => fiZZ , 4 => 4 , 5 buzz....100

function esDivisiblePor(numero, divisor) {
    return numero % divisor === 0;
}

function fizzbuzz(numero) {
    if (esDivisiblePor(numero, 3) && esDivisiblePor(numero, 5)) {
        return "FIZZBUZZ";
    }
    if (esDivisiblePor(numero, 3)) {
        return "FIZZ";
    }
    if (esDivisiblePor(numero, 5)) {
        return "BUZZ";
    }
    return numero;
}

function probarFizzBuzz(numeroEntrada, numeroSalida) {
    it.skip('fizzbuzz de ' + numeroEntrada + ' es ' + numeroSalida, () => {
        expect(fizzbuzz(numeroEntrada)).toBe(numeroSalida);
    });
}

probarFizzBuzz(1, 1);
probarFizzBuzz(2, 2);
probarFizzBuzz(3, "FIZZ");
probarFizzBuzz(4, 4);
probarFizzBuzz(5, "BUZZ");
probarFizzBuzz(6, "FIZZ");
probarFizzBuzz(9, "FIZZ");
probarFizzBuzz(10, "BUZZ");
probarFizzBuzz(11, 11);
probarFizzBuzz(12, "FIZZ");
probarFizzBuzz(15, "FIZZBUZZ");
probarFizzBuzz(45, "FIZZBUZZ");
probarFizzBuzz(30, "FIZZBUZZ");


