export default class Repositorio {
    constructor() {
        this.elementos = [
            {nombre: "Pedrito", apellido: "Perez"},
            {nombre: "Alfredo", apellido: "Rodriguez"},
            {nombre: "Alfonso", apellido: "Storni"}
        ];
    }

    listar() {
        return this.elementos;
    }
}

