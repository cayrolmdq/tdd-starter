export default class Servicio {

    constructor(repositorio) {
        this.repositorio = repositorio;
    }

    listarMinuscula() {
        let respuesta = [];
        const personaes = this.repositorio.listar();
        for (let i = 0; i < personaes.length; i++) {
            respuesta.push(personaes[i].nombre.toLowerCase());
        }
        return respuesta
    }
}
