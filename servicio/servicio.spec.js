import Servicio from './servicio';
import Repositorio from "./repositorio";

// UNITARIO
test('debe listar nombres en minuscula', function () {
    //Given
    const repositorioMock = {
        listar: jest.fn(),
    };
    repositorioMock.listar.mockReturnValue([{nombre: 'Maria José'}]);
    const servicio = new Servicio(repositorioMock);

    //When
    const actual = servicio.listarMinuscula();

    //Then
    expect(actual).toEqual(["maria josé"]);
    expect(repositorioMock.listar).toHaveBeenCalled();
});



// COMPONENTE
test('debe listar nombres en minuscula', function () {
    //Given
    const servicio = new Servicio(new Repositorio());

    //When
    const actual = servicio.listarMinuscula();

    //Then
    expect(actual).toEqual(["pedrito", "alfredo", "alfonso"]);
});